#include <iostream>
#include <cstring>
#include <gtkmm.h>
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmimgle/dcmimage.h"

using namespace std;

class MainWindow : public Gtk::Window {
    public:
	MainWindow(); 


    protected:
	// Signal handlers
	void select_file();
	void select_file_response(int response_id,
		Gtk::FileChooserDialog* openFileDialog);

	// Gtk attr
	Gtk::Label label;
	Gtk::Button select_file_button;

	// DCMTK attr
	OFCondition status;
	DcmFileFormat file_format;
	DcmDataset* dataset;
};

MainWindow::MainWindow()
    : label{"Click on button to choose a DICOM file:"},
    select_file_button{"Select DICOM file"}
{
    select_file_button.signal_clicked().connect(sigc::mem_fun(*this,
		&MainWindow::select_file));
    select_file_button.set_margin(100);
    set_child(select_file_button);
	
    set_title("Curupira Viewer");
    set_default_size(300, 300);
}


void MainWindow::select_file()
{
    auto openFileDialog = new Gtk::FileChooserDialog("Choose a file",
	    Gtk::FileChooser::Action::OPEN);
    openFileDialog->set_transient_for(*this);
    openFileDialog->set_modal(true);
    openFileDialog->add_button("Cancel", Gtk::ResponseType::CANCEL);
    openFileDialog->add_button("Open", Gtk::ResponseType::OK);
    openFileDialog->signal_response().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::select_file_response),
		openFileDialog));

    auto fileFilter = Gtk::FileFilter::create();
    fileFilter->set_name("DICOM Images (*.dcm)");
    fileFilter->add_pattern("*.dcm");
    openFileDialog->add_filter(fileFilter);
    fileFilter = Gtk::FileFilter::create();
    fileFilter->set_name("All Files (*.*)");
    fileFilter->add_pattern("*.*");
    openFileDialog->add_filter(fileFilter);

    cout << "Openned dialog" << endl;
    openFileDialog->show();
}

void MainWindow::select_file_response(int response_id,
	Gtk::FileChooserDialog* openFileDialog)
{
    cout << "Ran dialog response" << endl;
    if (response_id == Gtk::ResponseType::OK)
    {
	cout << "Got OK response" << endl;
	string filename = openFileDialog->get_file()->get_path();
	cout << "Opening " << filename << endl;
	status = file_format.loadFile(filename.c_str());
    }

    if (status.bad())
	cerr << "Problem openning file: " << openFileDialog->get_file()->get_path() << endl;

    delete openFileDialog;

    DcmDataset* dataset = file_format.getDataset();

    OFString patient_name;
    OFCondition condition;

    condition = dataset->findAndGetOFStringArray(DCM_PatientName, patient_name);

    if (condition.good())
    {
	label.set_text(string{"Patient name is: "} + (string)patient_name.data());
    }
    else
    {
	label.set_text("Could not get patient name");
    }
    set_child(label);
}

