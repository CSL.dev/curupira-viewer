# Curupira Viewer
A free as in freedom lightweight DICOM viewer.

## Dependencies
- ~~cmake >= 3.10~~
- meson
- ninja
- dcmtk lib
- gtkmm4 lib
- vtk lib

## Build
You can build using meson and ninja running:

```sh
$ meson build
$ cd build
$ ninja
```
## TODO
- [ ] Choose specific headers for gtkmm instead of "gtkmm.h", wich import all
