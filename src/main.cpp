#include <iostream>

#include <gtkmm.h>
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmimgle/dcmimage.h"

#include "main-window.hpp"

int main(int argc, char *argv[]) {
    auto app = Gtk::Application::create("org.curupiraviewer");
    return app->make_window_and_run<MainWindow>(argc, argv);
}
